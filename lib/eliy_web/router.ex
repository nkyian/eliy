defmodule EliyWeb.Router do
  use EliyWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", EliyWeb do
    pipe_through :api
  end
end
